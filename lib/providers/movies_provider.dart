// ignore_for_file: unnecessary_this, non_constant_identifier_names, avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:peliculas/models/now_playing_response.dart';
import 'package:peliculas/models/popular_response.dart';

class MoviesProvider extends ChangeNotifier {

  final String _apiKey = 'ea4dab698f11241ec3d21a4232457b62';
  final String _baseUrl = 'api.themoviedb.org';
  final String _language = 'es-ES';
  late NowPlayingResponse data;
  List<Movie> onDisplayMovies = [];
  List<Movie> popularMovies = [];

  int popularPage = 0;

  MoviesProvider() {
    print("MoviesProvider inicializado");
    this.getOnDisplayMovies();
    this.getPopularMovies();
  }

  Future<String> _getJsonData(String endpoint, {int page = 1}) async {
    Uri url = Uri.https(_baseUrl, endpoint, {
      'api_key': _apiKey,
      'language': _language,
      'page': '$page'
    });

    return (await http.get(url)).body;
  }

  getOnDisplayMovies() async {
    final jsonData = await this._getJsonData('3/movie/now_playing');
    data = NowPlayingResponse.fromMap(json.decode(jsonData));
    onDisplayMovies = data.results;
    notifyListeners();
  }

  getPopularMovies() async {
    popularPage++;
    final jsonData = await this._getJsonData('3/movie/popular', page: popularPage);
    final popularResponse = PopularResponse.fromJson(jsonData);
    popularMovies = [...popularMovies, ...popularResponse.results];
    notifyListeners();
  }
}